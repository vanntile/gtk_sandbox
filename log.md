# Log file with thoughts, observations, changes

## experiment/multipaned, 01.06.20

- `[MyMultipaned::on_size_allocate]` widget.compute_expand checks for widget.get_visible so there is no need for the second check
- `[MyMultipaned::on_size_allocate]` delta_minimums and delta_naturals don't have a value for each child, might break loops dont for children.size() iterations
- `[MyMultipaned::on_size_allocate]` useless double while (after testing)

```cpp
// useful when delta_total == 0
std::cout << "delta_total == 0" << std::endl;
std::cout << ((Gtk::Label*)((MyDropZone*)children[0])->get_child())->get_text() << std::endl;
std::for_each(children.begin(), children.end(), [](Gtk::Widget *child) { std::cout << child->get_allocation().get_width() << " "; });
std::cout << std::endl;
```

## experiment/multipaned, 02.06.20

- `[MyMultipaned::on_drag_update]` allocating less than the prefferred min_size will put out a warning, so added checks for that
