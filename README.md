# GTK_SANDBOX

Sandbox repo for gtk experiments. Each experiment will have its own
`experiment/` branch.

## Branch list

- [`experiment/multipaned`](https://gitlab.com/vanntile/gtk_sandbox/-/tree/experiment/multipaned)

## Run instructions

### Multipaned

```bash
# build command
g++ *.cpp -g -o paned `pkg-config gtkmm-3.0 --cflags --libs`

# To run with debug window:
GTK_DEBUG=interactive ./paned

# To run with x11 backend
GDK_BACKEND=x11 ./paned

# To run with wayland backend
GDK_BACKEND=wayland ./paned
```
